# Twitter API streamListener 
 Etienne Jacquot - 02/12/2021

## Getting Started:

Start by cloning this repository to your localhost:
- Pennkey SSO auth for Bitbucket requires sshkeys

```bash
git clone git@bitbucket.org:annenbergschool/twitterapi-sandydarity.git
```

## Deploying streamListener to get results

These stream listeners were originally deployed on ASC IT Production VM **NXITDockerSrv1** and the code was included in another repository. On 12/08/2020 the root partition on that server was fill and the collection of Twitter stream api results ended.

For clarity and reproducibility I have created a new repository exclusively for this project / these three twitter accounts.


### Running Docker locally (instead of the NXITDockerSrv VM)

- Confirm your local host has docker installed, more info [here](https://www.docker.com/get-started)

### Twitter Access Tokens:

- Before you proceed, please make sure you have Twitter access tokens readily available. If not, please visit [https://developer.twitter.com](https://developer.twitter.com) to create a developer account, an app, wait for app approval, and then generate your consumer & access tokens
	- For more information, please visit [here](https://developer.twitter.com/en/docs/basics/authentication/oauth-1-0a/obtaining-user-access-tokens)

- Once you have your access tokens, please save them as `/config/config.ini`
    - Please note, I added `*.ini` to the [.gitignore](.gitignore) to prevent my tokens from syncing to the repo
    - Format for the file:

```
[TWITTER]
consumer_key = [API-key]
consumer_secret = [API-secret-key]
access_key = [Access-token]
access_secret = [Access-token-secret]
```

## Simple-ETL for TwitterAPI to AWS Athena

In order to make this data available on AWS Athena for query, we need to cleanup the json files with the [ETL-simple](./etl-simple.ipynb) notebook...

- I ran this on my local Macbook, not NXITDocker or NXJHub given the json files are together nearly 1.5 GBs ...

- This takes the three files and combines into one Parquet outfile for upload from local `./data/etl/` to S3 bucket: `s3://asctwitterdata/etl-twitter-dvirgilio/`

- Note: I have used this for other twitter projects so just copying over the notebook to run for the three files!


_______________


## Directories:

- [bots](./bots)
    - [oversightboard](./bots/stream_oversightboard/) is main directory, script should be relatively automated given the Dockerfile specifies the Twitter input name
    - Has a [Dockerfile](./bots/dockerfile) which configures the stream listener, edit for specific username

- [timeline](./timeline)
    - [oversightboard](./timeline/timeline_oversightboard) is main directory with script, this should also be automated given Dockerfile has Twitter username of interest...
    - Has a [dockerfile](./timeline/dockerfile) that you **must edit for your specific twitter username of interest!**...

- [data](./data)
    - Directory to mount to docker container for output files... 
    - [stream_oversightboard](./data/stream_oversightboard') 
	- This is just a continuously written file in this directory
    - [timeline_oversightboard](./data/timeline_oversightboard') contains two directories for the dialy job...
    	- [raw](./data/stream_oversightboard/raw/) for json dumps of timeline so we have raw data
	- [etl](./data/stream_oversightboard/elt/) for parquet file which aims to replicate Wharton twitter historical db on AWS Athena

- [configs](./configs)
    - pip requirement text file
    - config.ini that you need to create with your Twitter access tokens...
    - cronjob example text which you should add to your host with `cronjob -e` and `systemctl status crond` to check that it's running
    
- [scripts](./scripts)
    - example scripts for bot & timeline containers ... Should be dependent on Dockerfile having an argument which passes Twitter Username... 
_______

## Example workflow for running Stream Listener for SandyDarity tweets: 

- **TODO**: the manually naming twitter user in one dockerfile isn't ideal...

- This containers dockerfile is located at [bots/dockerfile]

``` bash

cd /this/repo

# Review your dockerfile to confirm it's running correct bot for twitter username
cat bots/dockerfile

# check your app auth configs
cat configs/config.ini

# and make sure that your respective py scripts read the correct confg ini set:
cat bots/<twitter_username>/tweepy_stream.py | grep config

# To Docker build your container image run the following
# Running on CentOS8 host NXITDockerSrv1 needs explicit network flag

docker build --network=host -t sandydarity-tweepy-listener -f bots/dockerfile .

# To launch your tweepy listener container from your image
# Set your bind mount to a directory in the repo

docker run -it
	--network=host
	-v /mnt/itdevops_hdrive/BitBucket/twitterapi-sandydarity/data:/data
	--restart always
	sandydarity-tweepy-listener

# To check and confirm tweepy listeners are working

[ascroot@CentOS8 twitterapi-sandydarity]$ docker ps
CONTAINER ID        IMAGE                                    COMMAND                  CREATED             STATUS              PORTS               NAMES
8183520e9f18        sandydarity-tweepy-listener           "python3 bots/oversi…"   26 hours ago        Up 26 hours                             boring_robinson

# If a container goes down (it eventually will!) due to network drop or unknown error, attach the container to see stream output

[ascroot@CentOS8 twitterapi-sandydarity]$ docker attach boring_robinson 
@sandydarity ...
RT @sandydarity: ... 

# The output file is json dumped
# New outdir:

ls -l data/stream_sandydarity

# Old dir
[ascroot@CentOS8 twitterapi-sandydarity]$ ls -l data/ | grep sandydarity
-rwxr-xr-x. 1 ascroot root 36056 Jul  9 23:45 sandydarity_tweets.json

```

_________
